#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from abc import ABC, abstractmethod
from Resources.db_interaction import Db_Interaction


class DbObject(ABC):

    tableName = "dbObject"

    fieldList = {
        "id": "Int Not Null Default",
        "type": "VarChar(256) Not Null Default dbObject"
    }

    primaryKey = "id"

    #Empty if nothing has a foreign key
    foreignKeys = {}
    
    @abstractmethod
    def __init__(self):
        pass;
        
    def save(self):
        if self.inDatabase():
            self.update();
        else:
            self.insert();
    
    def update(self):
        print("Updating")
        #I don't know yet
        
    def insert(self):
        print("Inserting")
        #I don't know yet
        
    # def createTable(self):
    #     createSQL = "CREATE TABLE " + DbObject.tableName + " ("
    #     for key, field in self.fieldList:
    #         #Hmm
    #         if key == "type":
    #             createSQL += " " + field
    #         elif key == "default":
    #             createSQL += " " + field


    def giveTableName(self):
        return self.tableName

    def loadById(self, id):
        idKey = self.primaryKey
        tableName = self.tableName
        sql = "SELECT " + idKey + " FROM " + tableName + " WHERE "
        Db_Interaction.query(sql)
        return "NOT FINISHED"
