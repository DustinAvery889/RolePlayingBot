#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from Objects.dbObject import DbObject


class Character(DbObject):
    tableName = "character"

    fieldList = {
        "character_id": "Int NOT NULL NO DEFAULT",
        "name": "VarChar(256) NOT NULL DEFAULT defaultName",

        # Basic Attributes
        "str": "Int NOT NULL DEFAULT 10",
        "dex": "Int NOT NULL DEFAULT 10",
        "con": "Int NOT NULL DEFAULT 10",
        "intelligence": "Int NOT NULL DEFAULT 10",
        "wis": "Int NOT NULL DEFAULT 10",
        "cha": "Int NOT NULL DEFAULT 10",
    }

    primaryKey = "name"

    foreignKeys = {"dbObject": {"character_id":"id"}}

    def __init__(self, name):
        super().__init__();
        self.name = name;
