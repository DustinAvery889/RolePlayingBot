import mysql.connector
from mysql.connector import errorcode

#ToDo:Make this some sort of abstract thingy to use different databases
class Db_Interaction():
    set = False;

    config = {
        'host':"107.170.2.119",
        'username': "root",
        'password': "idk its on my desk",
        'database': "rolePlayingBot"
    }

    mysqlConnection = None
    mysqlCursor = None

    def __init__(self):
        self.set = True

        #Connect to database, or try to anyway
        try:
            Db_Interaction.mysqlConnection = mysql.connector.connect(**self.config)

        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database does not exist")
            else:
                print(err)

        else:
            Db_Interaction.mysqlConnection.close()

        #Gets the cursor, this is the thing doing SQL queries
        Db_Interaction.mysqlCursor = Db_Interaction.mysqlConnection.cursor()

    @staticmethod
    def query(sql, autocommit = False):
        Db_Interaction.mysqlCursor.execute(sql)
        if autocommit:
            Db_Interaction.commit()

    @staticmethod
    def commit():
        Db_Interaction.mysqlConnection.commit()

    @staticmethod
    def close():
        Db_Interaction.commit()
        Db_Interaction.mysqlConnection.close()

