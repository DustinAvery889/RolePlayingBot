from discord.ext import commands

from Modules.module import Module
from Objects.character import Character


class Debug(Module):

    def __init__(self, bot):
        super().__init__(bot)

    @commands.command()
    async def test(self):
        character = Character("Bob");

        result = character.giveTableName();
        await self.bot.say(result)


# Has to be in every command module
def setup(bot):
    bot.add_cog(Debug(bot));