#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from discord.ext import commands

from Modules.module import Module
from Objects.character import Character


class Characters(Module):

    def __init__(self, bot):
        super().__init__(bot)

    @commands.command()
    async def addCharacter(self, name):
        self.character = Character(name)
        self.character.save();


# Has to be in every command module        
def setup(bot):
    bot.add_cog(Characters(bot));
