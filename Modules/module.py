
class Module:
    def __init__(self, bot):
        self.bot = bot
        
    def checkForString(self, mString, substringInside):
        index=-1;
        try:
            index = mString.index(substringInside)
        except ValueError:
             return -1
             #            print("Debug Print, the substring was not there")
        return index
