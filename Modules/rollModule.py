#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import random

from discord.ext import commands

from Modules.module import Module


class Rolls(Module):

    def __init__(self, bot):
        super().__init__(bot)

    @commands.command()
    async def roll(self, dice : str, *options : str):
     """Rolls a dice in NdN format."""
     index = self.checkForString(dice, "d");
     if index == 0 and index != -1:
         nRoll = 1;
         #Empty string is a thing?
         limit = int(dice.split('d')[1]);
        
     elif index != -2:
         nRoll, limit = map(int,dice.split('d'));
        
     #TODO Options, need to be coded for, but yeah, we got options   
     print(", ".join(options));

     result = ', '.join(str(random.randint(1, limit)) for r in range(nRoll))
     await self.bot.say(result)
     

     
        
# Has to be in every command module        
def setup(bot):
    bot.add_cog(Rolls(bot));