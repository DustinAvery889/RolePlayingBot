# https://github.com/Rapptz/discord.py/blob/async/examples/reply.py
import random

import discord
from discord.ext import commands

TOKEN = 'NDIxOTY5MDU5Nzk0NTE4MDM3.DYU9Kg.MeQ0yGSHwbrvxL4S6h0lxd5uiAk'
description = '''An example bot to showcase the discord.ext.commands extension
module.
There are a number of utility commands being showcased here.'''

prefix = '$';

moduleList = ["rollModule", "characterModule", "debugModule"]

#A global variable, pretty much anything from this file can use this
bot = commands.Bot(command_prefix=prefix, description=description)

@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print("Bot Start Successful, enjoy Roleplaying fun")
    print('------')

@bot.event
async def on_message(message):
    #Classes and stuff for figuring what to do go here
#    trimmedMessage = message.content.strip();
#    channel = message.channel();
    print("on_message currently not working");
    await bot.process_commands(message);
            
    
    
#May not be used anymore
def checkForString(mainString, substringInside):
    index=-1;
    try:
        index = mainString.index(substringInside)
    except ValueError:
        return -1
#            print("Debug Print, the substring was not there")
    return index
    
#This should run commands take in options and stuff like that
def runCommand(command,optionsList):
    print("Something very wrong happened")
            
# Previous Code, how to do singular bot commands, on_message overwrites, prefer that anyway
@bot.command()
async def add(left : int, right : int):
     """Adds two numbers together."""
     await bot.say(left + right)
 
@bot.command()
async def load(extension_name : str):
    """Loads an extension."""
    try:
        bot.load_extension(extension_name)
    except (AttributeError, ImportError) as e:
        await bot.say("```py\n{}: {}\n```".format(type(e).__name__, str(e)))
        return
    await bot.say("{} loaded.".format(extension_name))

@bot.command()
async def unload(extension_name : str):
    """Unloads an extension."""
    bot.unload_extension(extension_name)
    await bot.say("{} unloaded.".format(extension_name))

@bot.command(description='For when you wanna settle the score some other way')
async def choose(*choices : str):
     """Chooses between multiple choices."""
     await bot.say(random.choice(choices))
 
@bot.command()
async def repeat(times : int, content='repeating...'):
     """Repeats a message multiple times."""
     for i in range(times):
         await bot.say(content)
 
@bot.command()
async def joined(member : discord.Member):
     """Says when a member joined."""
     await bot.say('{0.name} joined in {0.joined_at}'.format(member))
     
     

if __name__ == "__main__":
    for module in moduleList:
        try:
            bot.load_extension("Modules." + module)
        except Exception as e:
            exc = '{}: {}'.format(type(e).__name__, e)
            print('Failed to load module {}\n{}'.format(module, exc))

bot.run(TOKEN)